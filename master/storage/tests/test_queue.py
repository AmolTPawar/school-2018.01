from ..queue import NonVolatileQueue as Queue


root = "/tmp/test"

def test_put_and_dispose():
    q = Queue(rootdir=root)

    xs = list(reversed(range(100,1000)))

    for x in xs:
        assert q.put(str(x), x, x) is None
        assert q.contains(str(x))

    for x in reversed(xs):
        assert q.take() == (str(x), x)
        assert q.dispose(str(x)) is None
        assert not q.contains(str(x))

def test_duplicates():
    q = Queue(rootdir=root)

    for i in range(10):
        q.put("1", i, 1)

    assert q.take() == ("1", 1)
    assert q.take() is None

    q.dispose("1")

def test_overflow():
    q = Queue(rootdir=root, cache_size=10)

    xs = set(range(100,1000))

    for x in xs:
        q.put(str(x), x, x)

    vals = set()
    for _ in xs:
        key, value = q.take()
        assert key == str(value)
        vals |= {value}
    assert len(xs^vals) == 0

    for x in xs:
        q.dispose(str(x))

def test_contains(key="test"):
    q = Queue(rootdir=root)

    assert not q.contains(key)
    q.put(key, 0, key)
    assert q.contains(key)
    q.dispose(key)
    assert not q.contains(key)
